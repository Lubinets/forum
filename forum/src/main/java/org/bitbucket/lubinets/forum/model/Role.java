package org.bitbucket.lubinets.forum.model;

public enum Role {
    ROLE_USER(1),
    ROLE_MODERATOR(2),
    ROLE_ADMINISTRATOR(3);

    private final int id;

    Role(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
