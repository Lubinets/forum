package org.bitbucket.lubinets.forum.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "Post")
@Data
public class Post implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    
    @Column(name = "text")
    private String text;
    
    @ManyToOne
    @JoinColumn(name = "thread_id")
    private Thread thread;
    
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
