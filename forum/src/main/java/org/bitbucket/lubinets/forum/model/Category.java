package org.bitbucket.lubinets.forum.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "Category")
@Data
public class Category implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    
    @Column(name = "name")
    private String name;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "category")
    @Cascade({org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Thread> threads;   
}
