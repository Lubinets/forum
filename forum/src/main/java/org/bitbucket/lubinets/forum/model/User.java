package org.bitbucket.lubinets.forum.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "User")
@Data
public class User implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    
    @Column(name = "login")
    private String login;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "role")
    private Role role;
    
    @Column(name = "password")
    private String password;   
}
