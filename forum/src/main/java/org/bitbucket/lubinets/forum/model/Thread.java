package org.bitbucket.lubinets.forum.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.Cascade;

@Entity
@Table(name = "Thread")
@Data
public class Thread implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;
    
    @Column(name = "topic")
    private String topic;
    
    @ManyToOne
    @JoinColumn(name = "topic_starter_id")
    private User threadStarter;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "thread")
    @Cascade({org.hibernate.annotations.CascadeType.MERGE,
        org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Post> posts;
    
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
